import os

from flask_migrate import Migrate, upgrade

from app import create_app, db
from app.models import Company, Edu, OrgStructure, User, Role, Permission, \
    OfficeSeeker, Vacancy, Phone, Educational, sex, status, source, \
    qualification

app = create_app(os.getenv('FLASK_CONFIG') or 'default')
migrate = Migrate(app, db)


@app.shell_context_processor
def make_shell_context():
    return dict(db=db, Company=Company, Edu=Edu, OrgStructure=OrgStructure,
                User=User, Role=Role, Permission=Permission,
                OfficeSeeker=OfficeSeeker, Vacancy=Vacancy, Phone=Phone,
                Educational=Educational, sex=sex, status=status, source=source,
                qualification=qualification)


@app.cli.command()
def test():
    """Run the unit tests"""
    import unittest
    tests = unittest.TestLoader().discover('tests')
    unittest.TextTestRunner(verbosity=2).run(tests)


@app.cli.command()
def deploy():
    """Run deployment task."""
    # migrate database to latest version
    upgrade()

    # create or update user roles
    Role.insert_roles()
