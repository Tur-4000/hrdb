"""candidate

Revision ID: 212238c833b6
Revises: 5ea2a2c3edd0
Create Date: 2018-09-25 14:28:17.352611

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import mysql

# revision identifiers, used by Alembic.
revision = '212238c833b6'
down_revision = '5ea2a2c3edd0'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('office_seekers',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('register_date', sa.Date(), nullable=False),
    sa.Column('last_name', sa.String(length=64), nullable=False),
    sa.Column('first_name', sa.String(length=64), nullable=False),
    sa.Column('middle_name', sa.String(length=64), nullable=False),
    sa.Column('sex', sa.Integer(), nullable=False),
    sa.Column('birthdate', sa.Date(), nullable=True),
    sa.Column('email', sa.String(length=128), nullable=True),
    sa.Column('address', sa.Text(), nullable=True),
    sa.Column('status', sa.Integer(), nullable=False),
    sa.Column('source', sa.Integer(), nullable=False),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_index(op.f('ix_office_seekers_first_name'), 'office_seekers', ['first_name'], unique=False)
    op.create_index(op.f('ix_office_seekers_last_name'), 'office_seekers', ['last_name'], unique=False)
    op.create_index(op.f('ix_office_seekers_middle_name'), 'office_seekers', ['middle_name'], unique=False)
    op.create_table('educational',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('officeseeker_id', sa.Integer(), nullable=False),
    sa.Column('edu_id', sa.Integer(), nullable=False),
    sa.Column('profession', sa.String(length=64), nullable=True),
    sa.Column('qualification', sa.Integer(), nullable=True),
    sa.ForeignKeyConstraint(['edu_id'], ['education_establishment.id'], ),
    sa.ForeignKeyConstraint(['officeseeker_id'], ['office_seekers.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('job_vacancies',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('day_of_start', sa.Date(), nullable=False),
    sa.Column('day_of_end', sa.Date(), nullable=True),
    sa.Column('company_id', sa.Integer(), nullable=False),
    sa.Column('job_title', sa.String(length=140), nullable=False),
    sa.Column('requirement_education', sa.String(length=255), nullable=True),
    sa.Column('work_experience', sa.String(length=64), nullable=True),
    sa.ForeignKeyConstraint(['company_id'], ['companies.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_index(op.f('ix_job_vacancies_day_of_end'), 'job_vacancies', ['day_of_end'], unique=False)
    op.create_index(op.f('ix_job_vacancies_day_of_start'), 'job_vacancies', ['day_of_start'], unique=False)
    op.create_table('phones',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('phone_number', sa.String(length=32), nullable=False),
    sa.Column('officeseeker_id', sa.Integer(), nullable=False),
    sa.ForeignKeyConstraint(['officeseeker_id'], ['office_seekers.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_index(op.f('ix_phones_phone_number'), 'phones', ['phone_number'], unique=False)
    op.drop_index('ix_vacancy_day_of_end', table_name='vacancy')
    op.drop_index('ix_vacancy_day_of_start', table_name='vacancy')
    op.drop_table('vacancy')
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('vacancy',
    sa.Column('id', mysql.INTEGER(display_width=11), autoincrement=True, nullable=False),
    sa.Column('day_of_start', sa.DATE(), nullable=False),
    sa.Column('day_of_end', sa.DATE(), nullable=True),
    sa.Column('company_id', mysql.INTEGER(display_width=11), autoincrement=False, nullable=False),
    sa.Column('job_title', mysql.VARCHAR(collation='utf8_unicode_ci', length=140), nullable=False),
    sa.Column('requirement_education', mysql.VARCHAR(collation='utf8_unicode_ci', length=255), nullable=True),
    sa.Column('work_experience', mysql.VARCHAR(collation='utf8_unicode_ci', length=64), nullable=True),
    sa.ForeignKeyConstraint(['company_id'], ['companies.id'], name='vacancy_ibfk_1'),
    sa.PrimaryKeyConstraint('id'),
    mysql_collate='utf8_unicode_ci',
    mysql_default_charset='utf8',
    mysql_engine='InnoDB'
    )
    op.create_index('ix_vacancy_day_of_start', 'vacancy', ['day_of_start'], unique=False)
    op.create_index('ix_vacancy_day_of_end', 'vacancy', ['day_of_end'], unique=False)
    op.drop_index(op.f('ix_phones_phone_number'), table_name='phones')
    op.drop_table('phones')
    op.drop_index(op.f('ix_job_vacancies_day_of_start'), table_name='job_vacancies')
    op.drop_index(op.f('ix_job_vacancies_day_of_end'), table_name='job_vacancies')
    op.drop_table('job_vacancies')
    op.drop_table('educational')
    op.drop_index(op.f('ix_office_seekers_middle_name'), table_name='office_seekers')
    op.drop_index(op.f('ix_office_seekers_last_name'), table_name='office_seekers')
    op.drop_index(op.f('ix_office_seekers_first_name'), table_name='office_seekers')
    op.drop_table('office_seekers')
    # ### end Alembic commands ###
