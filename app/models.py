from datetime import datetime

from flask import current_app
from flask_login import UserMixin
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer
from werkzeug.security import generate_password_hash, check_password_hash

from . import db, login_manager


@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))


class Permission:
    READ = 1
    WRITE = 2
    ADMIN = 4


sex = [
    (0, '---'),
    (1, 'Женский'),
    (2, 'Мужской')
]


status = [
    (0, 'не собеседовался'),
    (1, 'подходит'),
    (2, 'исключить'),
    (3, 'резерв'),
]


source = [
    (0, 'внутренний'),
    (1, 'work.ua'),
    (2, 'headhunter'),
    (3, '0629'),
    (4, 'olx'),
    (5, 'rabota.ua'),
]


qualification = [
    (0, '---'),
    (1, 'среднее'),
    (2, 'среднее специальное'),
    (3, 'неоконченное высшее'),
    (4, 'высшее'),
]


class User(UserMixin, db.Model):
    """Пользователи"""
    __tablename__ = 'users'
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(64), unique=True, index=True)
    username = db.Column(db.String(64), unique=True, index=True, nullable=False)
    role_id = db.Column(db.Integer, db.ForeignKey('roles.id'))
    password_hash = db.Column(db.String(128))
    name = db.Column(db.String(64))
    last_seen = db.Column(db.DateTime(), default=datetime.utcnow)

    def __init__(self, **kwargs):
        super(User, self).__init__(**kwargs)
        if self.role is None:
            if self.email == current_app.config['HRDB_ADMIN']:
                self.role = Role.query.filter_by(name='Administrator').first()
            if self.role is None:
                self.role = Role.query.filter_by(default=True).first()

    def __repr__(self):
        return '<User %r>' % self.username

    @property
    def password(self):
        raise AttributeError('password is not readable attribute')

    @password.setter
    def password(self, password):
        self.password_hash = generate_password_hash(password)

    def verify_password(self, password):
        return check_password_hash(self.password_hash, password)

    def can(self, perm):
        return self.role is not None and self.role.has_permission(perm)

    def is_administrator(self):
        return self.can(Permission.ADMIN)

    def ping(self):
        self.last_seen = datetime.utcnow()
        db.session.add(self)
        db.session.commit()

    def generate_reset_token(self, expiration=3600):
        s = Serializer(current_app.config['SECRET_KEY'], expiration)
        return s.dumps({'reset': self.id}).decode('utf-8')

    @staticmethod
    def reset_password(token, new_password):
        s = Serializer(current_app.config['SECRET_KEY'])
        try:
            data = s.loads(token.encode('utf-8'))
        except:
            return False
        user = User.query.get(data.get('reset'))
        if user is None:
            return False
        user.password = new_password
        db.session.add(user)
        return True


class Role(db.Model):
    """Пользовательские роли"""
    __tablename__ = 'roles'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64), unique=True)
    default = db.Column(db.Boolean, default=False, index=True)
    permissions = db.Column(db.Integer)
    users = db.relationship('User', backref='role', lazy='dynamic')

    def __init__(self, **kwargs):
        super(Role, self).__init__(**kwargs)
        if self.permissions is None:
            self.permissions = 0

    def add_permission(self, perm):
        if not self.has_permission(perm):
            self.permissions += perm

    def remove_permission(self, perm):
        if self.has_permission(perm):
            self.permissions -= perm

    def reset_permissions(self):
        self.permissions = 0

    def has_permission(self, perm):
        return self.permissions & perm == perm

    @staticmethod
    def insert_roles():
        roles = {
            'User': [Permission.READ, Permission.WRITE],
            'Administrator': [Permission.READ, Permission.WRITE,
                              Permission.ADMIN]
        }
        default_role = 'User'
        for r in roles:
            role = Role.query.filter_by(name=r).first()
            if role is None:
                role = Role(name=r)
            role.reset_permissions()
            for perm in roles[r]:
                role.add_permission(perm)
            role.default = (role.name == default_role)
            db.session.add(role)
        db.session.commit()

    def __repr__(self):
        return '<Role %r>' % self.name


class Company(db.Model):
    """Справочник компаний входящих в группу"""
    __tablename__ = 'companies'
    id = db.Column(db.Integer, primary_key=True)
    c_name = db.Column(db.String(255), unique=True, index=True)
    c_short_name = db.Column(db.String(32), unique=True, index=True, nullable=False)
    department = db.relationship("OrgStructure",
                                 backref='company', lazy='dynamic')
    vacancies = db.relationship('Vacancy', backref='company', lazy='dynamic')

    def __repr__(self):
        return '<Компания %r>' % self.c_name


class OrgStructure(db.Model):
    """Справочник структурных подразделений"""
    __tablename__ = 'organization_structure'
    id = db.Column(db.Integer, primary_key=True)
    company_id = db.Column(db.Integer, db.ForeignKey('companies.id'))
    parent_id = db.Column(db.Integer,
                          db.ForeignKey('organization_structure.id'))
    ou = db.Column(db.String(255), index=True)
    children = db.relationship("OrgStructure",
                               backref=db.backref('parent', remote_side=[id]))

    def __repr__(self):
        return '<Организационное подразделение: %r>' % self.ou


class Edu(db.Model):
    """Справочник учебных заведений"""
    __tablename__ = 'education_establishment'
    id = db.Column(db.Integer, primary_key=True)
    e_name = db.Column(db.String(255), unique=True, index=True)
    e_short_name = db.Column(db.String(32), unique=True, index=True, nullable=False)
    educationals = db.relationship('Educational', backref='establishment',
                                   lazy='dynamic')

    def __repr__(self):
        return '<Учебное заведение: %r>' % self.e_name


class Vacancy(db.Model):
    """Таблица вакансий"""
    __tablename__ = 'job_vacancies'
    id = db.Column(db.Integer, primary_key=True)
    day_of_start = db.Column(db.Date, index=True, nullable=False)
    day_of_end = db.Column(db.Date, index=True)
    company_id = db.Column(db.Integer, db.ForeignKey('companies.id'), nullable=False)
    job_title = db.Column(db.String(140), nullable=False)
    requirement_education = db.Column(db.String(255))
    work_experience = db.Column(db.String(64))

    def __repr__(self):
        return '<Вакансия от {} в компанию {} на должность {}>'.format(
            self.day_of_start, self.company.c_short_name, self.job_title)


class OfficeSeeker(db.Model):
    """Таблица соискателей"""
    __tablename__ = 'office_seekers'
    id = db.Column(db.Integer, primary_key=True)
    register_date = db.Column(db.Date, nullable=False)
    last_name = db.Column(db.String(64), index=True, nullable=False)
    first_name = db.Column(db.String(64), index=True, nullable=False)
    middle_name = db.Column(db.String(64), index=True, nullable=False)
    sex = db.Column(db.Integer, nullable=False)
    birthdate = db.Column(db.Date)
    email = db.Column(db.String(128))
    address = db.Column(db.Text)
    status = db.Column(db.Integer, nullable=False)
    source = db.Column(db.Integer, nullable=False)
    phones = db.relationship('Phone', backref='officeseeker', lazy='dynamic')
    educationals = db.relationship('Educational', backref='officeseeker',
                                   lazy='dynamic')

    def __repr__(self):
        return f'<{self.last_name} {self.first_name} {self.middle_name}>'


class Phone(db.Model):
    """Телефонный справочник. Вспомогательная таблица"""
    __tablename__ = 'phones'
    id = db.Column(db.Integer, primary_key=True)
    phone_number = db.Column(db.String(32), index=True, nullable=False)
    officeseeker_id = db.Column(db.Integer,
                                db.ForeignKey('office_seekers.id'),
                                nullable=False)

    def __repr__(self):
        return '<т. {}>'.format(self.phone_number)


class Educational(db.Model):
    """Записи об образовании кандидатов. Вспомогательная таблица"""
    __tablename__ = 'educational'
    id = db.Column(db.Integer, primary_key=True)
    officeseeker_id = db.Column(db.Integer,
                                db.ForeignKey('office_seekers.id'),
                                nullable=False)
    edu_id = db.Column(db.Integer,
                       db.ForeignKey('education_establishment.id'),
                       nullable=False)
    profession = db.Column(db.String(64))
    qualification = db.Column(db.Integer)

    def __repr__(self):
        return '<Образование {} {}. {}. - {}>'.format(
            self.officeseeker.last_name,
            self.officeseeker.first_name[0],
            self.officeseeker.middle_name[0],
            self.establishment.e_short_name)
