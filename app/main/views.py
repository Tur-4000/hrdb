from flask import render_template, flash, redirect, url_for, request
from flask_login import login_required

from . import main
from .forms import AddCompanyForm, EditCompanyForm, AddEduForm, EditEduForm, \
                   DepartmentForm, VacancyForm, CloseVacancyForm, \
                   AddCandidateForm, EditCandidateForm, \
                   PhoneForm, DelPhoneForm, \
                   EducationalForm, DelEducationalForm
from .. import db
from ..models import Company, Edu, OrgStructure, Vacancy, OfficeSeeker, \
                     Phone, Educational, qualification, sex, status, source


@main.route('/')
def index():
    return render_template('index.html')


@main.route('/edu')
@login_required
def edu():
    edu = Edu.query.all()
    return render_template('list_edu.html', edu=edu)


@main.route('/add_edu', methods=['GET', 'POST'])
@login_required
def add_edu():
    form = AddEduForm()
    if form.validate_on_submit():
        edu = Edu(e_name=form.edu_full_name.data,
                  e_short_name=form.edu_short_name.data)
        db.session.add(edu)
        db.session.commit()
        flash('Новое учебное заведение добавлено')
        return redirect(url_for('main.edu'))
    return render_template('edu.html', form=form,
                           title='Добавление учебного заведения')


@main.route('/edit_edu/<id>', methods=['GET', 'POST'])
@login_required
def edit_edu(id):
    edu = Edu.query.filter_by(id=id).first_or_404()
    form = EditEduForm(edu=edu)
    if form.validate_on_submit():
        Edu.query.filter_by(id=int(form.id.data)).update(
            {'e_name': form.edu_full_name.data,
             'e_short_name': form.edu_short_name.data})
        db.session.commit()
        flash('Информация об учебном заведении изменена')
        return redirect(url_for('main.edu'))
    form.id.data = edu.id
    form.edu_full_name.data = edu.e_name
    form.edu_short_name.data = edu.e_short_name
    return render_template('edu.html', form=form, edu=edu,
                           title='Редактирование учебного заведения')


@main.route('/companies')
@login_required
def companies():
    companies = Company.query.all()
    return render_template('list_companies.html', companies=companies)


@main.route('/add_company', methods=['GET', 'POST'])
@login_required
def add_company():
    form = AddCompanyForm()
    if form.validate_on_submit():
        company = Company(c_name=form.company_full_name.data,
                          c_short_name=form.company_short_name.data)
        db.session.add(company)
        db.session.commit()
        flash('Новая компания добавлена')
        return redirect(url_for('main.companies'))
    return render_template('add_company.html', form=form)


@main.route('/edit_company/<id>', methods=['GET', 'POST'])
@login_required
def edit_company(id):
    company = Company.query.filter_by(id=id).first_or_404()
    form = EditCompanyForm(company=company)
    if form.validate_on_submit():
        Company.query.filter_by(id=int(form.id.data)).update(
                            {'c_name': form.company_full_name.data,
                             'c_short_name': form.company_short_name.data})
        db.session.commit()
        flash('Информация о компании изменена')
        return redirect(url_for('main.companies'))
    form.id.data = company.id
    form.company_full_name.data = company.c_name
    form.company_short_name.data = company.c_short_name
    return render_template('edit_company.html', form=form, company=company)


@main.route('/orgstructure')
@login_required
def orgstructure():
    departments = OrgStructure.query.all()
    return render_template('orgstructure.html', departments=departments)


@main.route('/add_department', methods=['GET', 'POST'])
@login_required
def add_department():
    form = DepartmentForm()
    if form.validate_on_submit():
        unit = OrgStructure(company_id=request.form['company'],
                            parent_id=request.form['parent'],
                            ou=form.children.data)
        db.session.add(unit)
        db.session.commit()
        flash(f'Подразделение "{form.children.data}" добавлено')
        return redirect(url_for('main.orgstructure'))
    return render_template('add_department.html', form=form)


@main.route('/edit_department/<id>', methods=['GET', 'POST'])
@login_required
def edit_department(id):
    form = DepartmentForm()
    unit = OrgStructure.query.filter_by(id=id).first_or_404()
    if form.validate_on_submit():
        # TODO: написать обработчик для сохранения редактирования отдела
        pass
    form.company.data = unit.company_id
    form.parent.data = unit.parent_id
    form.children.data = unit.ou
    return render_template('add_department.html', form=form)


@main.route('/vacancies')
@login_required
def list_vacancies():
    vacancies = Vacancy.query.all()
    return render_template('list_vacancies.html', vacancies=vacancies)


@main.route('/add_vacancy', methods=['GET', 'POST'])
@login_required
def add_vacancy():
    form = VacancyForm()
    if form.validate_on_submit():
        vacancy = Vacancy(day_of_start=form.day_of_start.data,
                          company_id=request.form['company'],
                          job_title=form.job_title.data,
                          requirement_education=form.education.data,
                          work_experience=form.work_experience.data,)
        db.session.add(vacancy)
        db.session.commit()
        return redirect(url_for('main.list_vacancies'))
    return render_template('vacancy.html', form=form, title='Добавить вакансию')


@main.route('/edit_vacancy/<int:id>', methods=['GET', 'POST'])
@login_required
def edit_vacancy(id):
    form = VacancyForm()
    vacancy = Vacancy.query.filter_by(id=id).first_or_404()
    if form.validate_on_submit():
        Vacancy.query.filter_by(id=id).update(
            {'day_of_start': form.day_of_start.data,
             'company_id': form.company.data,
             'job_title': form.job_title.data,
             'requirement_education': form.education.data,
             'work_experience': form.work_experience.data})
        db.session.commit()
        flash('Изменения сохранены')
        return redirect(url_for('main.list_vacancies'))
    form.day_of_start.data = vacancy.day_of_start
    form.company.data = vacancy.company_id
    form.job_title.data = vacancy.job_title
    form.education.data = vacancy.requirement_education
    form.work_experience.data = vacancy.work_experience
    return render_template('vacancy.html', form=form,
                           title='Редактировать вакансию', vacancy=vacancy)


@main.route('/close_vacancy/<int:id>', methods=['GET', 'POST'])
@login_required
def close_vacancy(id):
    form = CloseVacancyForm()
    vacancy = Vacancy.query.filter_by(id=id).first_or_404()
    if form.validate_on_submit():
        Vacancy.query.filter_by(id=id).update(
            {'day_of_end': form.day_of_end.data})
        db.session.commit()
        flash('Вакансия закрыта')
        return redirect(url_for('main.list_vacancies'))
    return render_template('close_vacancy.html', form=form, vacancy=vacancy,
                           title='Закрыть вакансию')


@main.route('/candidates')
@login_required
def list_candidates():
    candidates = OfficeSeeker.query.all()
    return render_template('list_candidates.html',
                           candidates=candidates, title='Список соискателей',
                           qualification=qualification, sex=sex, status=status,
                           source=source)


@main.route('/add_candidate', methods=['GET', 'POST'])
@login_required
def add_candidate():
    form = AddCandidateForm()
    if form.validate_on_submit():
        candidat = OfficeSeeker(
            register_date=form.register_date.data,
            last_name=form.last_name.data,
            first_name=form.first_name.data,
            middle_name=form.middle_name.data,
            sex=request.form['sex'],
            birthdate=form.birthdate.data,
            email=form.email.data,
            address=form.address.data,
            status=request.form['status'],
            source=request.form['source'],
        )
        db.session.add(candidat)
        db.session.commit()
        if not form.phone.data:
            pass
        else:
            phone = Phone(phone_number=form.phone.data,
                          officeseeker_id=candidat.id)
            db.session.add(phone)
        if request.form['qualification'] == '0':
            pass
        else:
            edu = Educational(officeseeker_id=candidat.id,
                              edu_id=request.form['edu'],
                              profession=form.profession.data,
                              qualification=int(request.form['qualification']))
            db.session.add(edu)
        db.session.commit()
        flash(f'Соискатель {form.last_name.data} {form.first_name.data} '
              f'{form.middle_name.data} добавлен')
        return redirect(url_for('main.list_candidates'))
    return render_template('candidate.html', form=form,
                           title='Добавить кандидата')


@main.route('/edit_candidate/<int:id>', methods=['GET', 'POST'])
@login_required
def edit_candidate(id):
    form = EditCandidateForm()
    candidate = OfficeSeeker.query.filter_by(id=id).first_or_404()
    if form.validate_on_submit():
        OfficeSeeker.query.filter_by(id=id).update(
            {'register_date': form.register_date.data,
             'last_name': form.last_name.data,
             'first_name': form.first_name.data,
             'middle_name': form.middle_name.data,
             'sex': request.form['sex'],
             'birthdate': form.birthdate.data,
             'email': form.email.data,
             'address': form.address.data,
             'status': request.form['status'],
             'source': request.form['source']})
        db.session.commit()
        flash(f'Соискатель {form.last_name.data} {form.first_name.data} '
              f'{form.middle_name.data} изменён')
        return redirect(url_for('main.list_candidates'))
    form.register_date.data = candidate.register_date
    form.last_name.data = candidate.last_name
    form.first_name.data = candidate.first_name
    form.middle_name.data = candidate.middle_name
    form.sex.data = candidate.sex
    form.birthdate.data = candidate.birthdate
    form.email.data = candidate.email
    form.address.data = candidate.address
    form.status.data = candidate.status
    form.source.data = candidate.source
    return render_template('candidate.html', form=form, candidate=candidate,
                           title='Редактировать кандидата',
                           qualification=qualification)


@main.route('/add_phone/<int:id>', methods=['GET', 'POST'])
@login_required
def add_phone(id):
    form = PhoneForm()
    if form.validate_on_submit():
        if not form.phone_number.data:
            pass
        else:
            phone = Phone(phone_number=form.phone_number.data,
                          officeseeker_id=id)
            db.session.add(phone)
            db.session.commit()
        flash(f'Номер {form.phone_number.data} добавлен')
        return redirect(url_for('main.list_candidates'))
    candidat = OfficeSeeker.query.filter_by(id=id).first_or_404()
    return render_template('phone.html', candidat=candidat, form=form,
                           title='Добавить телефон')


@main.route('/edit_phone/<int:candidat_id>/<int:phone_id>',
            methods=['GET', 'POST'])
@login_required
def edit_phone(candidat_id, phone_id):
    form = PhoneForm()
    if form.validate_on_submit():
        if not form.phone_number.data:
            pass
        else:
            Phone.query.filter_by(id=phone_id).update(
                {'phone_number': form.phone_number.data})
            db.session.commit()
        flash(f'Номер изменён')
        return redirect(url_for('main.list_candidates'))
    candidat = OfficeSeeker.query.filter_by(id=candidat_id).first_or_404()
    phone = Phone.query.filter_by(id=phone_id).first_or_404()
    form.phone_number.data = phone.phone_number
    return render_template('phone.html', candidat=candidat, form=form,
                           title='Изменить телефон')


@main.route('/del_phone/<int:phone_id>', methods=['GET', 'POST'])
@login_required
def del_phone(phone_id):
    phone = Phone.query.filter_by(id=phone_id).first_or_404()
    if request.method == 'POST':
        db.session.delete(phone)
        db.session.commit()
        flash(f'Номер {phone.phone_number} удалён')
        return redirect(url_for('main.list_candidates'))
    form = DelPhoneForm()
    form.id.data = phone.id
    return render_template('del_phone.html', phone=phone, form=form,
                           title='Удалить телефон')


@main.route('/add_educational/<int:id>', methods=['GET', 'POST'])
@login_required
def add_educational(id):
    form = EducationalForm()
    if form.validate_on_submit():
        if request.form['qualification'] == '0':
            return redirect(url_for('main.add_educational', id=id))
        else:
            edu = Educational(officeseeker_id=id,
                              edu_id=request.form['edu'],
                              profession=form.profession.data,
                              qualification=int(request.form['qualification']))
            db.session.add(edu)
            db.session.commit()
            flash('Запись об образовании добавлена')
            return redirect(url_for('main.list_candidates'))
    candidat = OfficeSeeker.query.filter_by(id=id).first_or_404()
    return render_template('educational.html', form=form, candidat=candidat,
                           title='Добавить запись об образовании')


@main.route('/edit_educational/<int:candidat_id>/<int:educational_id>',
            methods=['GET', 'POST'])
@login_required
def edit_educational(candidat_id, educational_id):
    form = EducationalForm()
    if form.validate_on_submit():
        if request.form['qualification'] == '0':
            return redirect(url_for('main.edit_educational', id=id))
        else:
            Educational.query.filter_by(id=educational_id).update({
                          'officeseeker_id': candidat_id,
                          'edu_id': request.form['edu'],
                          'profession': form.profession.data,
                          'qualification': int(request.form['qualification'])})
            db.session.commit()
            flash('Запись об образовании изменена')
            return redirect(url_for('main.list_candidates'))
    candidat = OfficeSeeker.query.filter_by(id=candidat_id).first_or_404()
    educational = Educational.query.filter_by(id=educational_id).first_or_404()
    form.edu.data = educational.edu_id
    form.profession.data = educational.profession
    form.qualification.data = educational.qualification
    return render_template('educational.html', form=form, candidat=candidat,
                           title='Изменить запись об образовании')


@main.route('/delete_educational/<int:educational_id>', methods=['GET', 'POST'])
@login_required
def delete_educational(educational_id):
    educational = Educational.query.filter_by(id=educational_id).first_or_404()
    if request.method == 'POST':
        db.session.delete(educational)
        db.session.commit()
        flash('Запись об образовании удалена')
        return redirect(url_for('main.list_candidates'))
    form = DelEducationalForm()
    form.id.data = educational.id
    return render_template('del_educational.html', educational=educational,
                           form=form, title='Удалить запись об образовании')
