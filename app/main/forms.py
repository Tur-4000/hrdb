from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, HiddenField, SelectField, \
                    TextAreaField
from wtforms.fields.html5 import DateField
from wtforms.validators import DataRequired, Length, Email, Regexp
from wtforms import ValidationError
from ..models import Company, Edu, OrgStructure, OfficeSeeker, \
                     sex, status, source, qualification


class AddCompanyForm(FlaskForm):
    company_full_name = StringField('Полное название компании',
                                    validators=[DataRequired(),
                                                Length(2, 255)])
    company_short_name = StringField('Краткое название компании',
                                     validators=[DataRequired(),
                                                 Length(2, 32)])
    submit = SubmitField('Сохранить')

    def validate_company_full_name(self, field):
        if Company.query.filter_by(c_name=field.data).first():
            raise ValidationError(
                'Компания с таким названием уже есть в базе.')

    def validate_company_short_name(self, field):
        if Company.query.filter_by(c_short_name=field.data).first():
            raise ValidationError(
                'Компания с таким названием уже есть в базе.')


class EditCompanyForm(FlaskForm):
    id = HiddenField('id')
    company_full_name = StringField('Полное название компании',
                                    validators=[DataRequired(),
                                                Length(2, 255)])
    company_short_name = StringField('Краткое название компании',
                                     validators=[DataRequired(),
                                                 Length(2, 32)])
    submit = SubmitField('Сохранить')

    def __init__(self, company, *args, **kwargs):
        super(EditCompanyForm, self).__init__(*args, **kwargs)
        self.company = company

    def validate_company_full_name(self, field):
        if field.data != self.company.c_name and \
                Company.query.filter_by(c_name=field.data).first():
            raise ValidationError(
                'Компания с таким названием уже есть в базе.')

    def validate_company_short_name(self, field):
        if field.data != self.company.c_short_name and \
                Company.query.filter_by(c_short_name=field.data).first():
            raise ValidationError(
                'Компания с таким названием уже есть в базе.')


class AddEduForm(FlaskForm):
    edu_full_name = StringField('Полное название учебного заведения',
                                    validators=[DataRequired(),
                                                Length(2, 255)])
    edu_short_name = StringField('Краткое название учебного заведения',
                                     validators=[DataRequired(),
                                                 Length(2, 32)])
    submit = SubmitField('Сохранить')

    def validate_edu_full_name(self, field):
        if Edu.query.filter_by(e_name=field.data).first():
            raise ValidationError(
                'Учебное заведение с таким названием уже есть в базе.')

    def validate_edu_short_name(self, field):
        if Edu.query.filter_by(e_short_name=field.data).first():
            raise ValidationError(
                'Учебное заведение с таким названием уже есть в базе.')


class EditEduForm(FlaskForm):
    id = HiddenField('id')
    edu_full_name = StringField('Полное название учебного заведения',
                                    validators=[DataRequired(),
                                                Length(2, 255)])
    edu_short_name = StringField('Краткое название учебного заведения',
                                     validators=[DataRequired(),
                                                 Length(2, 32)])
    submit = SubmitField('Сохранить')

    def __init__(self, edu, *args, **kwargs):
        super(EditEduForm, self).__init__(*args, **kwargs)
        self.edu = edu

    def validate_edu_full_name(self, field):
        if field.data != self.edu.e_name and \
                Edu.query.filter_by(e_name=field.data).first():
            raise ValidationError(
                'Учебное заведение с таким названием уже есть в базе.')

    def validate_edu_short_name(self, field):
        if field.data != self.edu.e_short_name and \
                Edu.query.filter_by(e_short_name=field.data).first():
            raise ValidationError(
                'Учебное заведение с таким названием уже есть в базе.')


class DepartmentForm(FlaskForm):
    company = SelectField('Компания', coerce=int)
    parent = SelectField('Вышестоящее подразделение', coerce=int)
    children = StringField('Подразделение', validators=[
                DataRequired(), Length(1, 255),
                Regexp('^[A-ZА-Яa-zа-я][A-ZА-Яa-zа-я0-9_. ]*$', 0,
                       'Название может содержать только буквы, цифры, точку '
                       'и символ подчеркивания')],
                           render_kw={'placeholder': 'Название подразделения'})
    submit = SubmitField('Сохранить')

    def __init__(self, *args, **kwargs):
        super(DepartmentForm, self).__init__(*args, **kwargs)
        self.company.choices = [(company.id, company.c_short_name)
             for company in Company.query.order_by(Company.c_short_name).all()]
        self.parent.choices = [(unit.id, " | ".join([unit.company.c_short_name, unit.ou]))
             for unit in OrgStructure.query.order_by(OrgStructure.ou).all()]
        # self.department = department


class VacancyForm(FlaskForm):
    day_of_start = DateField('Дата регистрации вакансии',
                             validators=[DataRequired()])
    company = SelectField('Компания', coerce=int)
    job_title = StringField('Должность',
                            validators=[DataRequired(), Length(3, 140)])
    education = StringField('Требования к образованию',
                            validators=[Length(0, 255)])
    work_experience = StringField('Требования к опыту работы',
                                  validators=[Length(0, 64)])
    submit = SubmitField('Сохранить')

    def __init__(self, *args, **kwargs):
        super(VacancyForm, self).__init__(*args, **kwargs)
        self.company.choices = [(company.id, company.c_short_name)
            for company in Company.query.order_by(Company.c_short_name).all()]

class CloseVacancyForm(FlaskForm):
    day_of_end = DateField('Дата закрытия вакансии',
                             validators=[DataRequired()])
    submit = SubmitField('Закрыть вакансию')


class EditCandidateForm(FlaskForm):
    register_date = DateField('Дата регистрации соискателя',
                              validators=[DataRequired()])
    last_name = StringField('Фамилия', validators=[DataRequired()])
    first_name = StringField('Имя', validators=[DataRequired()])
    middle_name = StringField('Отчество')
    sex = SelectField('Пол', coerce=int)
    birthdate = DateField('Дата рождения')
    email = StringField('eMail',
                        validators=[Length(0, 128), Email()])
    address = TextAreaField('Адрес')
    status = SelectField('Статус', coerce=int)
    source = SelectField('Источник поиска', coerce=int)
    submit = SubmitField('Сохранить')

    def __init__(self, *args, **kwargs):
        super(EditCandidateForm, self).__init__(*args, **kwargs)
        self.sex.choices = sex
        self.status.choices = status
        self.source.choices = source


class AddCandidateForm(EditCandidateForm):
    phone = StringField('Телефон')
    edu = SelectField('Учебное заведение', coerce=int)
    profession = StringField('Специальность')
    qualification = SelectField('Квалификация', coerce=int)

    def __init__(self, *args, **kwargs):
        super(AddCandidateForm, self).__init__(*args, **kwargs)
        self.sex.choices = sex
        self.status.choices = status
        self.source.choices = source
        self.qualification.choices = qualification
        self.edu.choices = [
            (edu.id, ' | '.join([edu.e_short_name, edu.e_name]))
            for edu in Edu.query.order_by(Edu.e_short_name).all()]


class PhoneForm(FlaskForm):
    phone_number = StringField('Номер телефона')
    submit = SubmitField('Сохранить')


class DelPhoneForm(FlaskForm):
    id = HiddenField('id')
    submit = SubmitField('Удалить')


class EducationalForm(FlaskForm):
    edu = SelectField('Учебное заведение', coerce=int)
    profession = StringField('Специальность')
    qualification = SelectField('Квалификация', coerce=int)
    submit = SubmitField('Сохранить')

    def __init__(self, *args, **kwargs):
        super(EducationalForm, self).__init__(*args, **kwargs)
        self.qualification.choices = qualification
        self.edu.choices = [(edu.id, ' | '.join([edu.e_short_name, edu.e_name]))
            for edu in Edu.query.order_by(Edu.e_short_name).all()]


class DelEducationalForm(FlaskForm):
    id = HiddenField('id')
    submit = SubmitField('Удалить')
