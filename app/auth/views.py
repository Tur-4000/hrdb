from flask import render_template, redirect, request, url_for, flash
from flask_login import logout_user, login_user, login_required, current_user

from . import auth
from .forms import LoginForm, RegistrationForm, EditUserProfileForm,\
                   ChangePasswordForm, PasswordResetRequestForm,\
                   PasswordResetForm
from .. import db
from ..decorators import admin_required
from ..models import User, Role
from ..email import send_email


@auth.before_app_request
def before_request():
    if current_user.is_authenticated:
        current_user.ping()


@auth.route('/login', methods=['GET', 'POST'])
def login():
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(username=form.username.data).first()
        if user is not None and user.verify_password(form.password.data):
            login_user(user, form.remember_me.data)
            next = request.args.get('next')
            if next is None or not next.startswith('/'):
                next = url_for('main.index')
            return redirect(next)
        flash('Неправильное имя или пароль')
    return render_template('auth/login.html', form=form)


@auth.route('/logout')
@login_required
def logout():
    logout_user()
    flash('Вы вышли из системы.')
    return redirect(url_for('main.index'))


@auth.route('/register', methods=['GET', 'POST'])
@login_required
@admin_required
def register():
    form = RegistrationForm()
    if form.validate_on_submit():
        user = User(email=form.email.data,
                    username=form.username.data,
                    password=form.password.data)
        db.session.add(user)
        db.session.commit()
        flash('Пользователь {} зарегистрирован.'.format(form.username.data))
        return redirect(url_for('main.index'))
    return render_template('auth/register.html', form=form)


@auth.route('/list_users')
@login_required
@admin_required
def list_users():
    users = User.query.all()
    return render_template('auth/list_users.html', users=users)


@auth.route('/edit_user/<id>', methods=['GET', 'POST'])
@login_required
@admin_required
def edit_user(id):
    user = User.query.filter_by(id=id).first_or_404()
    form = EditUserProfileForm(obj=user)
    form.role.choices = [(r.id, r.name) for r in Role.query.order_by('name')]
    if form.validate_on_submit():
        user.name = form.name.data
        user.username = form.username.data
        user.email = form.email.data
        user.role_id = request.form['role']
        db.session.commit()
        flash('Изменения сохранены')
        return redirect(url_for('auth.list_users'))
    form.role.default = user.role_id
    form.process()
    form.name.data = user.name
    form.username.data = user.username
    form.email.data = user.email
    return render_template('auth/edit_user.html', form=form)


@auth.route('/change_password', methods=['GET', 'POST'])
@login_required
def change_password():
    form = ChangePasswordForm()
    if form.validate_on_submit():
        if current_user.verify_password(form.old_password.data):
            current_user.password = form.password.data
            db.session.add(current_user)
            db.session.commit()
            flash('Ваш пароль изменён.')
            return redirect(url_for('main.index'))
        else:
            flash('Неправильный пароль.')
    return render_template('auth/change_password.html', form=form)


@auth.route('/reset', methods=['GET', 'POST'])
def password_reset_request():
    if not current_user.is_anonymous:
        return redirect(url_for('main.index'))
    form = PasswordResetRequestForm()
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data).first()
        if user:
            token = user.generate_reset_token()
            send_email(user.email, 'Восстановление пароля',
                       'auth/email/reset_password',
                       user=user, token=token)
        flash('Вам отправлено письмо с инструкцией по восстановлению пароля')
        return redirect(url_for('auth.login'))
    return render_template('auth/reset_password.html', form=form)


@auth.route('/reset/<token>', methods=['GET', 'POST'])
def password_reset(token):
    if not current_user.is_anonymous:
        return redirect(url_for('main.index'))
    form = PasswordResetForm()
    if form.validate_on_submit():
        if User.reset_password(token, form.password.data):
            db.session.commit()
            flash('Ваш пароль изменён.')
            return redirect(url_for('auth.login'))
        else:
            return redirect(url_for('main.index'))
    return render_template('auth/reset_password.html', form=form)
