from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, BooleanField, SubmitField, SelectField
from wtforms import ValidationError
from wtforms.validators import DataRequired, Length, Email, Regexp, EqualTo

from ..models import User


class LoginForm(FlaskForm):
    username = StringField('Логин',
                           validators=[DataRequired(), Length(1, 64)],
                           render_kw={'placeholder': 'Логин'})
    password = PasswordField('Пароль', validators=[DataRequired()],
                             render_kw={'placeholder': 'Пароль'})
    remember_me = BooleanField('Запомнить меня')
    submit = SubmitField('Войти')


class RegistrationForm(FlaskForm):
    email = StringField('Email',
                        validators=[DataRequired(), Length(1, 64), Email()],
                        render_kw={'placeholder': 'Email'})
    username = StringField('Имя пользователя', validators=[
        DataRequired(), Length(1, 64),
        Regexp('^[A-ZА-Яa-zа-я][A-ZА-Яa-zа-я0-9_.]*$', 0,
               'Имя пользователя может содержать только буквы, цифры, точку '
               'и символ подчеркивания')],
                           render_kw={'placeholder': 'Имя пользователя'})
    password = PasswordField('Пароль', validators=[
        DataRequired(), EqualTo('password2', message='Пароли должны совпадать')],
                             render_kw={'placeholder': 'Пароль'})
    password2 = PasswordField('Подтверждение пароля', validators=[DataRequired()],
                              render_kw={'placeholder': 'Подтверждение пароля'})
    submit = SubmitField('Зарегистрировать')

    def validate_email(self, field):
        if User.query.filter_by(email=field.data).first():
            raise ValidationError('Такой eMail уже зарегистрирован.')

    def validate_username(self, field):
        if User.query.filter_by(username=field.data).first():
            raise ValidationError('Такое имя пользователя уже используется')


class EditUserProfileForm(FlaskForm):
    name = StringField('Имя пользователя', validators=[
                                        DataRequired(), Length(1, 64)],
                       render_kw={'placeholder': 'Имя пользователя'})
    username = StringField('Логин', validators=[
        DataRequired(), Length(1, 64),
        Regexp('^[A-ZА-Яa-zа-я][A-ZА-Яa-zа-я0-9_.]*$', 0,
               'Логин может содержать только буквы, цифры, точку '
               'и символ подчеркивания')],
                           render_kw={'placeholder': 'Логин'})
    email = StringField('Email',
                        validators=[DataRequired(), Length(1, 64), Email()],
                        render_kw={'placeholder': 'Email'})
    role = SelectField('Роль', coerce=int)
    submit = SubmitField('Сохранить')

    def validate_username(self, field):
        if User.query.filter_by(username=field.data).first() and \
                (User.query.filter_by(username=field.data).first().username !=
                 field.data):
            raise ValidationError('Такой логин уже используется')

    def validate_email(self, field):
        if User.query.filter_by(email=field.data).first() and \
                (User.query.filter_by(email=field.data).first().email !=
                 field.data):
            raise ValidationError('Такой eMail уже зарегистрирован.')


class ChangePasswordForm(FlaskForm):
    old_password = PasswordField('Старый пароль', validators=[DataRequired()])
    password = PasswordField('Новый пароль', validators=[
        DataRequired(), EqualTo('password2', message='Пароли должны совпадать.')])
    password2 = PasswordField('Подтверждение пароля',
                              validators=[DataRequired()])
    submit = SubmitField('Изменить пароль')


class PasswordResetRequestForm(FlaskForm):
    email = StringField('Email', validators=[DataRequired(), Length(1, 64),
                                             Email()])
    submit = SubmitField('Восстановить пароль')


class PasswordResetForm(FlaskForm):
    password = PasswordField('Новый пароль', validators=[
        DataRequired(), EqualTo('password2', message='Пароля должны совпадать')])
    password2 = PasswordField('Подтверждение пароля', validators=[DataRequired()])
    submit = SubmitField('Изменить пароль')
